$(function () {

    $.fn.extend({
        animateCss: function (animationName, callback) {
            var animationEnd = (function (el) {
                var animations = {
                    animation: 'animationend',
                    OAnimation: 'oAnimationEnd',
                    MozAnimation: 'mozAnimationEnd',
                    WebkitAnimation: 'webkitAnimationEnd',
                };

                for (var t in animations) {
                    if (el.style[t] !== undefined) {
                        return animations[t];
                    }
                }
            })(document.createElement('div'));

            this.addClass('animated ' + animationName).one(animationEnd, function () {
                $(this).removeClass('animated ' + animationName);

                if (typeof callback === 'function') callback();
            });

            return this;
        },
    });
    
    $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
        return elementBottom > viewportTop && elementTop < viewportBottom;
    };
    
    //Check if viewport is mobile
    var isMobile = false;
    
    if($(".hamburger").css("display") == "block") {
        isMobile = true;
    }
    
    $(".gallery img").each(function() {
        if($(this).isInViewport()) {
            $(this).css("opacity", "1");
        }
    });

    $(".hamburger").click(function () {
        $(".mobile-nav.links").css("display", "block");
        $(".mobile-nav.links").animateCss("slideInLeft faster");
        
        $(".exit").animateCss("fadeIn");
        $(".exit").css("display", "block");
        
        $(document).mouseup(function (e){
            
            if($(".mobile-nav.links").css("display") == "none") {
                return;
            }

            var container = $(".mobile-nav.links");

            if (!container.is(e.target) && container.has(e.target).length === 0){

                $(".mobile-nav.links").animateCss("slideOutLeft faster", function() {
                    $(".mobile-nav.links").css("display", "none");
                });

                $(".exit").animateCss("fadeOut faster", function() {
                        $(".exit").css("display", "none");
                });

            }
        }); 
    });

    $(".exit").click(function () {
        $(".mobile-nav.links").animateCss("slideOutLeft faster", function() {
            $(".mobile-nav.links").css("display", "none");
        });
        
        $(".exit").animateCss("fadeOut faster", function() {
                $(".exit").css("display", "none");
        });
        
        console.log("Hiding menu");
    });
    
    $(".add-on .inner").each(function(){
         $(this).css("opacity", "0");
    });
    
    $(window).scroll(function() {
        
        var windowHeight = $(window).height(),
        gridTop = windowHeight * .2;
        gridBottom = windowHeight * .8;


        $('.service').each(function() {
            var thisTop = $(this).offset().top - $(window).scrollTop();

            if (thisTop >= gridTop && (thisTop + $(this).height()) <= gridBottom) {
                $(this).find("h2").css("color", "#e6b201");
                $(this).find(".yellow").css("width", "115px");
                $(this).find(".yellow").css("height", "115px");
                $(this).find(".yellow img").css("background-color", "#e6b201");
            } else {
                $(this).find("h2").css("color", "#000");
                $(this).find(".yellow").css("width", "100px");
                $(this).find(".yellow").css("height", "100px");
                $(this).find(".yellow img").css("background-color", "#facd5b");
            }
        });
        
        $(".package").each(function() {
            var thisTop = $(this).offset().top - $(window).scrollTop();

            if (thisTop >= gridTop && (thisTop + $(this).height()) <= gridBottom) {
                $(this).find("h1").css("color", "#e6b201");
                $(this).find("img").css("width", "175px");
                $(this).find("img").css("height", "175px");
            } else {
                $(this).find("h1").css("color", "#000");
                $(this).find("img").css("width", "150px");
                $(this).find("img").css("height", "150px");
            }
            
        });
        
        $(".add-on .inner").each(function(){
            var thisTop = $(this).offset().top - $(window).scrollTop();

            if (thisTop >= gridTop && (thisTop + $(this).height()) <= gridBottom) {
                $(this).css("opacity", "1");
            }
        });
        
        $(".about").each(function() {
            if($(this).isInViewport()) {
                $(this).css("opacity", "1");
            }
        });
        
        $(".about-img").each(function() {
            if($(this).isInViewport()) {
                $(this).css("opacity", "1");
            }
        });
        
        $(".divider").each(function() {
            if($(this).isInViewport()) {
                $(this).css("opacity", "1");
            }
        });
        
        $(".footer ul").each(function() {
            if($(this).isInViewport()) {
                $(this).css("opacity", "1");
            }
        });
        
        $(".gallery img").each(function() {
            if($(this).isInViewport()) {
                $(this).css("opacity", "1");
            }
        });
        
        
    });
    
    var modal = document.getElementById('myModal');
    
    var modalImg = document.getElementById("img01");
    
    $(".gallery img").click(function() {
        modal.style.display = "block";
        modalImg.src = $(this).get()[0].src;
        $(modalImg).css("opacity", "1");
    });

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    if(span) {
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() { 
            modal.style.display = "none";
        }
    }
    
});
















